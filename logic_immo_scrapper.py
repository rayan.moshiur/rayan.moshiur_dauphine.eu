import requests
import pandas as pd
import json
pd.set_option('display.width', None)
from time import sleep
from bs4 import BeautifulSoup
import re

url = "https://www.logic-immo.com/vente-immobilier-ile-de-france,1_0/options/groupprptypesids=1,2,6,7,12"
userAgent = ("Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17")
headers = {"User-Agent": userAgent}
resp = requests.get(url, headers=headers)
html_parser = BeautifulSoup(resp.content, "html.parser")

results = []

def checkUserInput(input):
	try:
        	value = int(input)
	except ValueError:
		try :
			print("La valeur choisie est incorrecte")
			print("Veuillez choisir le nombre de pages allant être consultees sur logic-immo, chaque page faisant 20 annonces (min 1 - max 500)")
			nbPage = input()
			checkUserInput(nbPage)
		except TypeError :
			print("La valeur choisie est incorrecte")
			print("Veuillez choisir le nombre de pages allant être consultees sur logic-immo, chaque page faisant 20 annonces (min 1 - max 500)")
			nbPage = input()
			checkUserInput(nbPage)

def numberOfPages():
	listPagination = html_parser.find(class_="listPagination")
	maxPage = listPagination.find_all("li")[-2].text.strip()
	print(maxPage)

def realEstateDataExtraction(url):
	resp = requests.get(url, headers=headers)
	html_parser = BeautifulSoup(resp.content, "html.parser")

	# Find products
	products = html_parser.find_all(class_="announceBox")


	for product in products :
		title = " ".join(product.find(class_="announceDtlInfosPropertyType").stripped_strings).split()[0]
		price = " ".join(product.find(class_="announceDtlPrice").text.strip().split()).rsplit(" ", 1)[0]
		price = price.replace(" ", "")
		city = product.find(class_="announcePropertyLocation").text.strip().rsplit(" ", 1)[0].rstrip()
		postal = product.find(class_="announcePropertyLocation").text.strip().split()[-1][1:-1]
		surface = product.find(class_="announceDtlInfosArea").text.strip().split("m")[0]
		if (len(product.find(class_="announceDtlInfosNbRooms").text.strip()) > 1) :
			rooms = product.find(class_="announceDtlInfosNbRooms").text.strip().split()[0]
		else :
			 rooms = "no data"
		if(price == '' or surface == ''):
			priceBySurface = 0
		else:
			priceBySurface = int(price) / int(surface)
		link = product.find(class_="linkToFa")['href']
		results.append({"title": title, "price": price, "city": city, "postal": postal, "surface": surface, "rooms": rooms, "priceBySurface": priceBySurface, "link": link})


def findAveragePrice(estateData):

	priceByPostals = []

	estateData = estateData.reset_index()

	for ind in estateData.index :
		if estateData["title"][ind] == "Studio":
			estateData["title"][ind] = "Appartement"

		if re.match("^[0-9]{5}$", str(estateData["postal"][ind])) :
			url = "http://api.cquest.org/dvf?code_postal="+str(estateData["postal"][ind])+"&type_local="+str(estateData["title"][ind]).capitalize()
			userAgent = ("Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17")
			headers = {"User-Agent": userAgent}
			resp = requests.get(url, headers=headers)
			html_parser = BeautifulSoup(resp.content, "html.parser")

			data = json.loads(str(html_parser))
			df2 = pd.DataFrame.from_dict(data["resultats"])

			if "valeur_fonciere" in df2.columns :
				df2 = df2[["date_mutation", "nature_mutation", "valeur_fonciere", "surface_lot_1", "nombre_lots", "nombre_pieces_principales"]].query('date_mutation > "2017-01-01"')
				df2 = df2.dropna()
				df2["prix_m2"] = df2["valeur_fonciere"] / df2["surface_lot_1"]
				df2["valeur_pieces"] = df2["valeur_fonciere"] / df2["nombre_pieces_principales"]
				avgPrix_m2 = df2["prix_m2"].mean()
				priceByPostals.append({"postal" : estateData["postal"][ind], "title" : estateData["title"][ind], "avgPrice" : avgPrix_m2})

			else:
				df.drop(df.index[df['postal'] == str(estateData["postal"][ind])], inplace=True)

	priceByPostalsDf = pd.DataFrame(priceByPostals)
	return priceByPostalsDf


def findingGoodDeals(adsDf, avgPriceDf):

	deals = []

	for ind1 in adsDf.index :
		if adsDf["title"][ind1] == "Studio" :
			adsDf["title"][ind1] = "Appartement"
		for ind2 in avgPriceDf.index :
			if adsDf["postal"][ind1] == avgPriceDf["postal"][ind2] and adsDf["title"][ind1] == avgPriceDf["title"][ind2] and adsDf["priceBySurface"][ind1] < avgPriceDf["avgPrice"][ind2] :
				deals.append({"title": adsDf["title"][ind1], "price": adsDf["price"][ind1], "city": adsDf["city"][ind1], "postal": adsDf["postal"][ind1], "surface": adsDf["surface"][ind1], "rooms": adsDf["rooms"][ind1], "priceBySurface": round(adsDf["priceBySurface"][ind1], 2), "avgPrice": round(avgPriceDf["avgPrice"][ind2], 2), "difference": round(avgPriceDf["avgPrice"][ind2]-adsDf["priceBySurface"][ind1], 2)})

	dealsDf = pd.DataFrame(deals)
	dealsDf = dealsDf.sort_values(by=['difference'])
	return dealsDf


def main():
	global df
	print("L'objectif de ce programme est de recuperer un certain nombre d'annonces\nde ventes de biens immobiliers actuellement en ligne sur logic-immo.com,\nd'en recuperer les informations, d'observer la valeur du marche actuel,\net de proposer les annonces les plus interessantes en terme d'investissement.")
	print("Pour commencer, je vous propose de definir l'echantillon d'annonces que vous souhaitez etudier")
	print("Veuillez choisir le nombre de pages allant etre consultees sur logic-immo, chaque page faisant 20 annonces (min 1 - max 500)")
	nbPage = input()
	while not nbPage.isdigit() or int(nbPage) > 500 or int(nbPage) < 1:
		print("La valeur choisie est incorrecte")
		print("Veuillez choisir le nombre de pages allant être consultees sur logic-immo, chaque page faisant 20 annonces (min 1 - max 500)")
		nbPage = input()
	nbPage = int(nbPage)

	for page in range(1, nbPage+1) :
		nextPageUrl = url+"/page="+str(page)
		realEstateDataExtraction(nextPageUrl)

	# Save in xlsx
	df = pd.DataFrame(results)
	xlsx_file = "elements_parcourus.xlsx"
	df.to_excel(xlsx_file, index=False)

	df = pd.read_excel("elements_parcourus.xlsx")
	titlesAndPostals = df[["title", "postal"]]
	titlesAndPostals = titlesAndPostals.drop_duplicates()

	averagePrice = findAveragePrice(titlesAndPostals)

	goodDeals = findingGoodDeals(df, averagePrice)

	print("\n")
	print("******************************************La recherche fut un succes !********************************************")
	print("Veuillez retrouver les annonces les plus interessantes sur le marche dans le fichier 'annonces_interessantes.xlsx'")
	print("Merci pour votre appel a notre outil et a bientot !")
	print("******************************************************************************************************************")
	sleep(0.5)
	dfGoodDeals = pd.DataFrame(goodDeals)
	xlsx_file = "annonces_interessantes.xlsx"
	dfGoodDeals.to_excel(xlsx_file, index=False)

main()
